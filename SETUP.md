#JENKINS SERVER

##ARCHITECTURE SETUP

Jenkins Server is deployed to AWS via **Cloudformation template** *jenkins.yml*.

Setup consists of a single ec2 instance.

###AWS::Cloudformation Template Configuration

####UserData:
  In this field install dependencies as follows:
  node, python, aws-cfn, docker(node might be unnecessary)
  then run aws-cfn-init command as follows:
  /usr/local/bin/cfn-init -v -s <stackname> -r "<ResourceName(ex. JenkinsServer)>" --region "ap-southeast-2"

#####AWS::Clouformation::Init:
  After running **cfn-init** from **UserData** this field will run commands.
  Commands to be run:
  1. Get Jenkins pkg library
  2. Add it to sources list
  3. Update packages
  4. Install Java
  5. Install Jenkins 
  6. Add jenkins to docker group(might be unnecessary)
  7. Start Jenkins Service
  

##DEPLOYMENT INSTRUCTIONS

//TODO

